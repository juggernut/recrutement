__author__ = 'juggernut'
# coding=utf-8
from django.db import models


class Commune(models.Model):
    concat_key = models.CharField(max_length=10, primary_key=True)
    c_insee = models.CharField(max_length=5, db_column='C_INSEE', null=False )
    ll_com = models.CharField(max_length=60, db_column='LL_COM', null=False)
    lc_com = models.CharField(max_length=32, db_column='LC_COM')
    c_postal = models.CharField(max_length=5, db_column='C_POSTAL', null=False)
    c_dep = models.CharField(max_length=3, db_column='C_DEP', null=False)
    d_creation = models.DateField(db_column='D_CREATION', null=False)
    d_modification = models.DateField(db_column='D_MODIFICATION', null=False)
    d_deb_val = models.DateField(db_column='D_DEB_VAL')
    d_fin_val = models.DateField(db_column='D_FIN_VAL')
    particularite_commune = models.CharField(max_length=1, db_column='PARTICULARITE_COMMUNE')
    c_postal_principal = models.CharField(max_length=5, db_column='C_POSTAL_PRINCIPAL')
    ligne_acheminement = models.CharField(max_length=60, db_column='LIGNE_ACHEMINEMENT')

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.concat_key:
            self.concat_key = str(self.c_insee) + str(self.c_postal)
        super(Commune, self).save(force_insert, force_update, using, update_fields)

    class Meta:
        db_table = "C_COMMUNE"


class Composante(models.Model):
    cod_cmp = models.CharField(max_length=3, primary_key=True, db_column='COD_CMP', null=False)
    lib_cmp = models.CharField(max_length=3, db_column='LIB_CMP', null=False)
    lic_cmp = models.CharField(max_length=3, db_column='LIC_CMP', null=False)

    class Meta:
        db_table = "C_COMPOSANTE"


class Elp(models.Model):
    cod_elp = models.CharField(max_length=3, primary_key=True, db_column='COD_ELP', null=False)
    cod_cmp = models.ForeignKey('Composante', db_column='COD_CMP', null=False)
    lib_elp = models.CharField(max_length=60,  db_column='LIB_ELP', null=False)
    lic_elp = models.CharField(max_length=25,  db_column='LIC_ELP', null=False)

    class Meta:
        db_table = "C_ELEMENT_PEDAGOGI"


class Etape(models.Model):
    cod_etp = models.CharField(max_length=3, primary_key=True, db_column='COD_ETP', null=False)
    lib_etp = models.CharField(max_length=60,  db_column='LIB_ETP', null=False)
    lic_etp = models.CharField(max_length=25,  db_column='LIC_ETP', null=False)

    class Meta:
        db_table = "C_ETAPE"


class Pays(models.Model):
    c_pays = models.CharField(max_length=3, primary_key=True, db_column='C_PAYS', null=False)
    ll_pays = models.CharField(max_length=40, db_column='LL_PAYS')
    lc_pays = models.CharField(max_length=20, db_column='LC_PAYS')
    l_nationalite = models.CharField(max_length=20, db_column='L_NATIONALITE')
    d_deb_val = models.DateField(db_column='D_DEB_VAL')
    d_fin_val = models.DateField(db_column='D_FIN_VAL')
    d_creation = models.DateField(db_column='D_CREATION', null=False)
    d_modification = models.DateField(db_column='D_MODIFICATION', null=False)
    code_iso = models.CharField(max_length=2, db_column='CODE_ISO')
    l_edition = models.CharField(max_length=40, db_column='L_EDITION')
    ll_pays_en = models.CharField(max_length=40, db_column='LL_PAYS_EN')
    iso_3166_3 = models.CharField(max_length=3, db_column='ISO_3166_3')

    class Meta:
        db_table = "C_PAYS"

    def __unicode__(self):
        return self.lib_pay


class VdiFractionnerVet(models.Model):
    concat_key = models.CharField(max_length=19, primary_key=True)
    cod_etp = models.CharField(max_length=6, db_column='COD_ETP', null=False)
    cod_vrs_vet = models.IntegerField(db_column='COD_VRS_VET', null=False)
    cod_dip = models.CharField(max_length=7, db_column='COD_DIP', null=False)
    cod_vrs_vdi = models.IntegerField(db_column='COD_VRS_VDI', null=False)
    cod_sis_daa_min = models.IntegerField(db_column='COD_SIS_DAA_MIN')
    cod_sis_daa_max = models.IntegerField(db_column='COD_SIS_DAA_MAX')
    daa_deb_rct_vet = models.CharField(max_length=4, db_column='DAA_DEB_RCT_VET', null=False)
    daa_deb_val_vet = models.CharField(max_length=4, db_column='DAA_DEB_VAL_VET', null=False)
    daa_fin_rct_vet = models.CharField(max_length=4, db_column='DAA_FIN_RCT_VET', null=False)
    daa_fin_val_vet = models.CharField(max_length=4, db_column='DAA_FIN_VAL_VET', null=False)
    pds_vet_vdi_vde = models.IntegerField(db_column='PDS_VET_VDI_VDE')

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.concat_key:
            self.concat_key = str(self.cod_etp)+str(self.cod_vrs_vet)+str(self.cod_dip)+str(self.cod_vrs_vdi)
        super(VdiFractionnerVet, self).save(force_insert, force_update, using, update_fields)

    class Meta:
        db_table = "C_VDI_FRACTIONNER_VET"


class VersionDiplome(models.Model):
    concat_key = models.CharField(max_length=19, primary_key=True)
    cod_dip = models.CharField(max_length=7, db_column='COD_VRS_VDI', null=False)
    cod_vrs_vdi = models.IntegerField(db_column='COD_ETP', null=False)
    lic_vdi = models.CharField(max_length=7, db_column='LIC_VDI', null=False)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.concat_key:
            self.concat_key = str(self.cod_dip)+ str(self.cod_vrs_vdi)
        super(VersionDiplome, self).save(force_insert, force_update, using, update_fields)

    class Meta:
        db_table = "C_VERSION_DIPLOME"


class VersionEtape(models.Model):
    concat_key = models.CharField(max_length=19, primary_key=True)
    cod_etp = models.CharField(max_length=6, db_column='COD_ETP', null=False)
    cod_vrs_vet = models.IntegerField(db_column='COD_VRS_VET', null=False)
    lib_cmt_vet = models.CharField(max_length=2000, db_column='LIB_CMT_VET')

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.concat_key:
            self.concat_key = str(self.cod_etp)+ str(self.cod_vrs_vet)
        super(VersionEtape, self).save(force_insert, force_update, using, update_fields)

    class Meta:
        db_table = "C_VERSION_ETAPE"


class Voirie(models.Model):
    c_voie = models.CharField(max_length=4, primary_key=True, null=False, db_column='C_VOIE')
    l_voie = models.CharField(max_length=30, db_column='L_VOIE')
    d_creation = models.DateField(null=False, db_column='D_CREATION')
    d_modification = models.DateField(null=False, db_column='D_MODIFICATION')

    class Meta:
        db_table = "C_VOIRIE"






