# coding=utf-8
from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from nomenclature.models import Pays

path_to_upload ="test/"


class Individu(models.Model):
    GENDER_CHOICES = (
        ('M', 'Homme'),
        ('F', 'Femme'),
    )
    user = models.OneToOneField(User, null=True, related_name='user_personne')
    last_name = models.CharField(u"Nom patronymique", max_length=30, null=True)
    common_name = models.CharField(u"Nom d'époux", max_length=30, null=True,
                                   blank=True)
    first_name1 = models.CharField(u"Prénom", max_length=30)
    first_name2 = models.CharField(u"Deuxième prénom", max_length=30, null=True,
                                   blank=True)
    first_name3 = models.CharField(u"Troisième prénom", max_length=30, null=True,
                                   blank=True)
    personal_email = models.EmailField("Email", unique=True, null=True)
    date_registration_current_year = models.DateTimeField(auto_now_add=True)
    sex = models.CharField(u'sexe', max_length=1, choices=GENDER_CHOICES, null=True)
    birthday = models.DateField('date de naissance', null=True)
    etape = models.CharField("etape", max_length=30, default="personne_info")
    status = models.ManyToManyField('Status', through='StatusPersonne')
    imposible_status = models.ForeignKey('Status', verbose_name=u"status imposible",
                                         related_name="status_imposible_personne",
                                         null=True, blank=True)
    code_pays_birth = models.ForeignKey(
        to=Pays,
        verbose_name=u"Pays de naissance",
        related_name=u"pays_naissance_personne2",
        default=None,
        null=True
    )
    town_birth = models.CharField(
        verbose_name=u'Ville naissance',
        max_length=30,
        null=True,
        blank=True,
    )
    mange_id = models.IntegerField(null=True)

    class Meta:
        db_table = "INDIVIDU"


class IndividuMessage(models.Model):
    individu = models.ForeignKey(Individu)
    date = models.DateField(auto_now=True)
    objet = models.CharField(max_length=200)
    message = models.TextField()

    class Meta:
        db_table = "INDIVIDU_MESSAGE"


class TypeAdresse(models.Model):
    label = models.CharField(max_length=200)

    class Meta:
        db_table = 'TYPE_ADRESSE'


class Status(models.Model):
    label = models.CharField(max_length=100)
    imposible_charge_cours = models.BooleanField(default=False)

    def __unicode__(self):
        return unicode(self.label)

    class Meta:
        db_table = "STATUT"


class StatusPersonne(models.Model):
    status = models.ForeignKey('Status')
    personne = models.ForeignKey('Individu')
    annee = models.ForeignKey('Annee')

    class Meta:
        db_table = "STATUS_PERSONNE"


class Annee(models.Model):
    annee = models.IntegerField(primary_key=True)


class Question(models.Model):
    status = models.ForeignKey(Status)
    label = models.TextField()


class Fichier(models.Model):
    question = models.ForeignKey(Question)
    fichier = models.FileField(upload_to='data')


class AdresseIndividu(models.Model):
    """c'est l'adresse d'individu
    aussi bien l'adresse étrangère que française
    """

    listed_number = models.CharField(u'Numero de teléphone :', max_length=15)
    #obligatoire
    individu = models.ForeignKey(u'Individu', related_name='adresses')
    code_pays = models.ForeignKey(Pays, verbose_name='Pays :', related_name='apogee_pays')
    label_adr_1 = models.CharField(u'Adresse :', max_length=32, null=False)
    label_adr_2 = models.CharField(u"Suite de l'adresse :", max_length=32,
                                   null=True, blank=True)
    label_adr_3 = models.CharField(u"Suite de l'adresse :", max_length=32,
                                   null=True, blank=True)
    cod_bdi = models.CharField(max_length=5, null=True)
    cod_com = models.CharField(max_length=5, null=True)

    label_adr_etr = models.CharField(u"Code postal et ville étrangère :",
                                     max_length=32, null=True, blank=True)
    annee = models.ForeignKey(Annee)

    class Meta:
        db_table = "ADRESSE"


class Candidature(models.Model):
    ec = models.ForeignKey('EC')
    personne = models.ForeignKey(Individu)
    autoriser_candidater = models.BooleanField(default=False)
    etape = models.CharField(max_length=20, default="debut_ec")
    code_dossier = models.CharField(max_length=20, primary_key=True)
    cv_file = models.FileField(upload_to="test/", null=True)
    lm_file = models.FileField(upload_to="test/", null=True)
    annee =  models.ForeignKey(Annee)

    class Meta:
        db_table = "CANDIDATURE"


class OffreEc(models.Model):
    candidature = models.ForeignKey(Candidature)
    candidature_heure = models.IntegerField()


class EC(models.Model):
    #to do later
    pass