{% load rst %}
.. _{{ table|upper }}:

=========================================================
{{ table|upper }}
=========================================================

{% colonne_tableau len_colonne 3%}
{{ "Nom du champ"|do_case:len_colonne }} {{ "Type du champ"|do_case:len_colonne }} {{ "relation"|do_case:len_colonne }}
{% colonne_tableau len_colonne 3%}
{% for ligne in lignes %}{{ ligne.0|do_case:len_colonne|safe }} {{ ligne.1|do_case:len_colonne|safe }} {{ ligne.2|do_case:len_colonne|safe }}
{% endfor %}
{% colonne_tableau len_colonne 3%}

{% if lignes_m2m %}
-------------------------------------------------------
RELATION m2m
-------------------------------------------------------

{% colonne_tableau len_colonne 2 %}
{{ "Nom de la table"|do_case:len_colonne }} {{ "Nom du champ" }}
{% colonne_tableau len_colonne 2 %}
{% for ligne in lignes_m2m %}{{ ligne.0|do_case:len_colonne|safe }} {{ ligne.1|do_case:len_colonne|safe }}
{% endfor %}
{% colonne_tableau len_colonne 2%}
{% endif %}
